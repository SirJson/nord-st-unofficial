# Unoffical st (suckless term) patches for the Nord base16 color theme

Sadly I couldn't find an official Nord theme for st so I went and created my own.

The basis for this theme was the official [.Xresources Version](https://github.com/arcticicestudio/nord-xresources). I also included the color palette as C #defines in case you don't like the standard arrangement of colors.

## Installation

You can either use:

- **nord-theme-def.path** ➜ Great for a new install because it patches the config template.
- **nord-theme.path** ➜ In case you already have a config file and don't want to start new.

***

It really comes down to "Do I already have a config.h or not?". The results are the same.

Enjoy! :diamond_shape_with_a_dot_inside:

#### And for the people who didn't read the title. This is NOT the official theme (if there ever will be one) and I'm not associated with Arctic Ice Studio. In case we get an official Nord st this repository will be obsolete and archived.

# Can I help you?

It looks like everything is working. After all porting 16 colors isnt't that hard. But if you have a merge request or feedback I will take a look. 
Maybe I got something wrong while porting or maybe you made it work with st version *<Insert future version number here>* or any other case I can't think of right now.